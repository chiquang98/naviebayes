/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classifymail;

import ProcessingText.Convert;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilterWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author chiquang
 */
public class ClassifyMailForView {

    int[] dicSpam, dicNonSpam;
    double[] ApSpamX0, ApSpamX1, ApNonSpamX0, ApNonSpamX1;
    int countNonSpam = 0;
    int countSpam = 0;
    double pSpam, pNonSpam;
    int countNonSpamPredict = 0, countSpamPredict = 0;
    int[] label;
//    int lenArr = Convert.listWord.size();
    /**
     * @param args the command line arguments
     */
    int lenDic;
    Convert c;

    public ClassifyMailForView() {
        c = new Convert();
        c.readDictionary();
        lenDic = c.getlenListWord();
    }

    public void readLabel() {
        System.out.println(lenDic);
        File fileLabel = new File("data/training/train-labels.txt");
        BufferedReader read = null;
        label = new int[lenDic + 2];//so luong email la so luong label
        try {
            read = new BufferedReader(new FileReader(fileLabel));
            String line = "";
            int cnt = 1;
            while ((line = read.readLine()) != null) {
                if (line.compareToIgnoreCase("0") == 0) {
                    countNonSpam++;
                    label[cnt] = 0;
                } else {
                    countSpam++;
                    label[cnt] = 1;
                }
                cnt++;
            }

        } catch (FileNotFoundException ex) {
//            Logger.getLogger(ClassifyMail.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
//            Logger.getLogger(ClassifyMail.class.getName()).log(Level.SEVERE, null, ex);
        }
        pNonSpam = (double) (countNonSpam + 1) / (double) (countNonSpam + countSpam + 1);
        pSpam = (double) (countSpam + 1) / (double) (countNonSpam + countSpam + 1);
    }

    public void readFeature() {
        dicSpam = new int[lenDic + 2];//co 2500 tu trong tu dien
        dicNonSpam = new int[lenDic + 2];
        for (int i = 0; i < lenDic + 2; i++) {
            dicSpam[i] = 0;
            dicNonSpam[i] = 0;
        }
        File readFeature = new File("data/training/train-features.txt");
        BufferedReader read = null;

        try {
            read = new BufferedReader(new FileReader(readFeature));
            String line = "";
            int numberMail = 0;
            while ((line = read.readLine()) != null) {
                String arr[] = line.split(" ");
                numberMail = Integer.parseInt(arr[0]);
                if (label[numberMail] == 0) {
                    dicNonSpam[Integer.parseInt(arr[1])]++;// tong so x=1|C=non spam0
                } else {
                    dicSpam[Integer.parseInt(arr[1])]++;//tong so x = 1 |C= spam
                }

            }
        } catch (FileNotFoundException ex) {
//            Logger.getLogger(ClassifyMail.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
//            Logger.getLogger(ClassifyMail.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void caculateProbality() throws IOException {//training
        //phai tinh ca nhung cai khong xuat hien
        //cacultateFor Non Spam
        File outputTrain1 = new File("data/training/trainNonSpam-p.txt");
        File outputTrain2 = new File("data/training/trainSpam-p.txt");
        BufferedWriter writerNonSpam = null, writerSpam = null;
        try {
            writerNonSpam = new BufferedWriter(new FileWriter(outputTrain1));
            writerSpam = new BufferedWriter(new FileWriter(outputTrain2));
        } catch (IOException ex) {
//            Logger.getLogger(ClassifyMail.class.getName()).log(Level.SEVERE, null, ex);
        }
        double probalityX0 = 0d, probalityX1 = 0d;//X0 tuc la X=0|Cj, X1 la X=1|Cj
        double mauNonSpam = countNonSpam + 1;
        double mauSpam = countSpam + 1;
        String pattern = "";
        for (int i = 1; i <= lenDic; i++) {//2500 la so tu trong tu dien |V|
            //cot dau tien la X =0, cot thu 2 la X = 1
            if (dicNonSpam[i] == 0) {
                probalityX0 = (double) (countNonSpam + 1) / (double) mauNonSpam;
                probalityX1 = (double) 1 / (double) mauNonSpam;
                pattern = Double.toString(probalityX0) + " " + Double.toString(probalityX1);
                writerNonSpam.write(pattern);
                writerNonSpam.newLine();
            } else {
                probalityX0 = (double) (countNonSpam - dicNonSpam[i] + 1) / (double) mauNonSpam;
                probalityX1 = (double) (dicNonSpam[i] + 1) / (double) mauNonSpam;
                pattern = Double.toString(probalityX0) + " " + Double.toString(probalityX1);
                writerNonSpam.write(pattern);
                writerNonSpam.newLine();
            }

            if (dicSpam[i] == 0) {
                probalityX0 = (double) (countSpam + 1) / (double) mauSpam;
                probalityX1 = (double) 1 / (double) mauSpam;
                pattern = Double.toString(probalityX0) + " " + Double.toString(probalityX1);
                writerSpam.write(pattern);
                writerSpam.newLine();
            } else {
                probalityX0 = (double) (countSpam - dicSpam[i] + 1) / (double) mauSpam;
                probalityX1 = (double) (dicSpam[i] + 1) / (double) mauSpam;
                pattern = Double.toString(probalityX0) + " " + Double.toString(probalityX1);
                writerSpam.write(pattern);
                writerSpam.newLine();
            }
        }
        writerNonSpam.close();
        writerSpam.close();

    }
    int[] testLabel, predictLabel;

    public void getTestLabel() {
        File fileLabel = new File("data/test/test-labels.txt");
        BufferedReader read = null;
        int cnt = 1;
        testLabel = new int[lenDic + 2];

        try {
            read = new BufferedReader(new FileReader(fileLabel));
            String line = "";

            while ((line = read.readLine()) != null) {
                if (line.compareToIgnoreCase("0") == 0) {
                    countNonSpamPredict++;
                } else {
                    countSpamPredict++;
                }
                testLabel[cnt] = Integer.parseInt(line);
                cnt++;
            }

        } catch (FileNotFoundException ex) {
//            Logger.getLogger(ClassifyMail.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
//            Logger.getLogger(ClassifyMail.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void getProbality() {
        File FileFeatureNonSpam = new File("data/training/trainNonSpam-p.txt");
        BufferedReader readNonSpam = null;
        File FileFeatureSpam = new File("data/training/trainSpam-p.txt");
        BufferedReader readSpam = null;

        try {
            readNonSpam = new BufferedReader(new FileReader(FileFeatureNonSpam));
            readSpam = new BufferedReader(new FileReader(FileFeatureSpam));
            ApNonSpamX0 = new double[lenDic + 2];
            ApNonSpamX1 = new double[lenDic + 2];
            ApSpamX0 = new double[lenDic + 2];
            ApSpamX1 = new double[lenDic + 2];
            String lineSpam = "", lineNonSpam = null;
            int cnt = 1;
            double numX0, numX1;
            while ((lineSpam = readSpam.readLine()) != null && (lineNonSpam = readNonSpam.readLine()) != null) {
                String[] temp = lineNonSpam.split(" ");
                numX0 = Double.parseDouble(temp[0]);//vi cot dau tien cho X =0 
                numX1 = Double.parseDouble(temp[1]);
                ApNonSpamX0[cnt] = numX0;
                ApNonSpamX1[cnt] = numX1;
                temp = lineSpam.split(" ");
                numX0 = Double.parseDouble(temp[0]);
                numX1 = Double.parseDouble(temp[1]);
                ApSpamX0[cnt] = numX0;
                ApSpamX1[cnt] = numX1;
                cnt++;
            }
        } catch (FileNotFoundException ex) {
//            Logger.getLogger(ClassifyMail.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
//            Logger.getLogger(ClassifyMail.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    double dochinhxac = 0d;

    public double getDochinhxac() {
        return dochinhxac;
    }

    public void training() {
        readFeature();
        try {
            caculateProbality();
        } catch (IOException ex) {
            Logger.getLogger(ClassifyMailForView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void getAccuracy() {

    }

    public int fit(String mailFeature) {
        getProbality();
//        getTestLabel();
        predictLabel = new int[lenDic + 2];
        File readFeature = new File(mailFeature);
        BufferedReader read = null;
        double classifySpam = Math.log(pSpam), classifyNonSpam = Math.log(pNonSpam);
        String result = "";
        double tempSpam, tempNonSpam;
        int flag = 0;
        try {
            int cnt = 1;
            read = new BufferedReader(new FileReader(readFeature));
            String line = "";
            double initNonSpam = 0d, initSpam = 0d;
            for (int i = 1; i <= lenDic; i++) {
                initNonSpam += Math.log(ApNonSpamX0[i]);
                initSpam += Math.log(ApSpamX0[i]);
            }
            while ((line = read.readLine()) != null) {

                String arr[] = line.split(" ");
                String sttMail = arr[0];

                if (Integer.parseInt(arr[0]) != cnt) {

                    classifyNonSpam += initNonSpam;
                    classifySpam += initSpam;

                    if (classifyNonSpam > classifySpam) {//suy ra non spam
                        result = "Mail number " + cnt + " is non spam";
                        predictLabel[cnt] = 0;
//                        System.out.println(result);
                        flag = 0;

                    } else {
                        result = "Mail number " + cnt + " is spam";
//                        System.out.println(result);
                        predictLabel[cnt] = 1;
                        flag = 1;
                    }
                    cnt++;
                    classifySpam = Math.log(pSpam);
                    classifyNonSpam = Math.log(pNonSpam);

                    classifyNonSpam += Math.log(ApNonSpamX1[Integer.parseInt(arr[1])]);
                    classifySpam += Math.log(ApSpamX1[Integer.parseInt(arr[1])]);
                    classifyNonSpam -= Math.log(ApNonSpamX0[Integer.parseInt(arr[1])]);
                    classifySpam -= Math.log(ApSpamX0[Integer.parseInt(arr[1])]);
                } else {
                    classifyNonSpam += Math.log(ApNonSpamX1[Integer.parseInt(arr[1])]);
                    classifySpam += Math.log(ApSpamX1[Integer.parseInt(arr[1])]);
                    classifyNonSpam -= Math.log(ApNonSpamX0[Integer.parseInt(arr[1])]);
                    classifySpam -= Math.log(ApSpamX0[Integer.parseInt(arr[1])]);
                }

            }
            if (classifyNonSpam > classifySpam) {//suy ra non spam
                result = "Mail number " + cnt + " is non spam";
                predictLabel[cnt] = 0;
//                System.out.println(result);
                flag = 0;
            } else {
                result = "Mail number " + cnt + " is spam";
//                System.out.println(result);
                predictLabel[cnt] = 1;
                flag = 1;
            }
        } catch (FileNotFoundException ex) {
//            Logger.getLogger(ClassifyMail.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
//            Logger.getLogger(ClassifyMail.class.getName()).log(Level.SEVERE, null, ex);
        }

        //caculate accuracy
        int len = countNonSpamPredict + countSpamPredict;

        int acc = 0;
        for (int i = 1; i <= len; i++) {
            if (predictLabel[i] == testLabel[i]) {
                acc++;
            }
        }
//        System.out.println(acc + "|" + len);
        this.dochinhxac = (double) acc / ((double) (len));
//        System.out.println("Do chinh xac la: " + dochinhxac * 100 + "%");
        return flag;
    }

    public static void main(String[] args) throws IOException {
//        ClassifyMail1 temp = new ClassifyMail1();
//        temp.readLabel();
//        temp.readFeature();
//        temp.caculateProbality();
//        temp.fit();

    }

}
