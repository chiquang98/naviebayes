/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProcessingText;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilterWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import model.Word;

/**
 *
 * @author Ryan
 */
public class Convert {

    public  List<String> listWord;
    private final String dictionary = "data/dictionary/dictionary.txt";
//    private final String emailFolder = "D:/Document/Artificial Intelligence/Emails/test";
//    private final String spamFolder = "F:/ProjectJava/ClassifyMail/data/demotrain/enron1/spam";
//    private final String hamFolder = "F:/ProjectJava/ClassifyMail/data/demotrain/enron1/ham";
//    private final String testSpamFolder = "F:/ProjectJava/ClassifyMail/data/demotrain/enron1/test-Spam";
//    private final String testHamFolder = "F:/ProjectJava/ClassifyMail/data/demotrain/enron1/test-Ham";
//    private final String featureFileTrain = "F:/ProjectJava/ClassifyMail/data/demotrain/enron1/train-features.txt";
////    private final String LabelFileTrain = "F:/ProjectJava/ClassifyMail/data/demotrain/enron1";
//    private final String featureFileTest = "F:/ProjectJava/ClassifyMail/data/demotrain/enron1/test-features.txt";
////    private final String LabelFileTest = "F:/ProjectJava/ClassifyMail/data/demotrain/enron1";

    public Convert() {
        listWord = new ArrayList<>();
    }

    public List<String> getListWord() {
        return listWord;
    }
    public int getlenListWord(){
        return listWord.size();
    }
    //Read and add word to word list
    public int readDictionary() {
//        System.out.println("Reading dictionary...");
        File file = new File(dictionary);
        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String s = reader.readLine();
            while (s != null && s.length() > 1) {
                listWord.add(s);
                s = reader.readLine();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return listWord.size();
    }

    public void writeLabel(String folderHam,String folderSpam,String outputProcessed) {
        File labelHam = new File(folderHam);
        File labelSpam = new File(folderSpam);
        File outLabelTrain = new File(outputProcessed);
        File[] listFile = labelHam.listFiles();//Ham truoc roi den Spam
        int temp = listFile.length;
        listFile = labelSpam.listFiles();
        int sum = temp + listFile.length;
        BufferedWriter writer = null;
        System.out.println(sum);
        try {

            writer = new BufferedWriter(new FileWriter(outLabelTrain));
            for (int i = 1; i <= sum; i++) {
                if (i <= temp) {
                    writer.write("0");
                } else {
                    writer.write("1");
                }
                writer.newLine();
            }
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(Convert.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    //Convert folder to a file
    public void convertAMailToFeature(String outFeature, String inputMail) {
        readDictionary();
        FileWriter fw = null;
        try {
            //Create or clear feature file
            File featureTrain = new File(outFeature);
            fw = new FileWriter(featureTrain);
            fw.write("");
            fw.close();
            fw = new FileWriter(featureTrain, true);
            //Read all file from email folder

            int cnt = 1;

            File file = new File(inputMail);
            String content = "";
            //Create a list word for current file
            List<Word> listWordInFile = new ArrayList<>();
            try {
                content = readFile(file);
                //Split into words
                String words[] = content.split("\\W+|\\d+");
                for (String word : words) {
                    if (word.length() > 1) {
                        Word w = getWordInListWordInFile(word, listWordInFile);
                        if (w == null) {
                            //If word is in word list then search in dictionary for the id
                            int id = getIdFromDictionary(word);
                            if (id >= 0) {
                                w = new Word(id, word, 1);
                                listWordInFile.add(w);
                            }
                        } else {
                            w.setTime(w.getTime() + 1);
                        }
                    }
                }
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
            //Write to file
            for (Word w : listWordInFile) {
                fw.write((cnt) + " " + (w.getId() + 1) + " " + w.getTime() + "\n");
//                    System.out.print((cnt ) + " " + w.getId() + 1 + " " + w.getTime() + "\n");
            }

        } catch (IOException ex) {
            Logger.getLogger(Convert.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fw.close();
            } catch (IOException ex) {
                Logger.getLogger(Convert.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void convertToFeatureList(String hamFolder,String spamFolder,String outputFeature) {
        System.out.println("Converting");
        try {
            //Create or clear feature file
            File featureTrain = new File(outputFeature);

            FileWriter fw = new FileWriter(featureTrain);
//            fw.write("");
//            fw.close();
//            fw = new FileWriter(featureTrain, true);
            //Read all file from email folder
            File folderHam = new File(hamFolder);
            File folderSpam = new File(spamFolder);
            File[] listFile = folderHam.listFiles();
            int cnt = 1;
            for (int i = 0; i < listFile.length; i++) {
                File file = listFile[i];
                String content = "";
                //Create a list word for current file
                List<Word> listWordInFile = new ArrayList<>();
                try {
                    content = readFile(file);
                    //Split into words
                    String words[] = content.split("\\W+|\\d+");
                    for (String word : words) {
                        if (word.length() > 1) {
                            Word w = getWordInListWordInFile(word, listWordInFile);
                            if (w == null) {
                                //If word is in word list then search in dictionary for the id
                                int id = getIdFromDictionary(word);
                                if (id >= 0) {
                                    w = new Word(id, word, 1);
                                    listWordInFile.add(w);
                                }
                            } else {
                                w.setTime(w.getTime() + 1);
                            }
                        }
                    }
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
                //Write to file
                for (Word w : listWordInFile) {
                    fw.write((cnt) + " " + (w.getId() + 1) + " " + w.getTime() + "\n");
//                    System.out.print((cnt ) + " " + w.getId() + 1 + " " + w.getTime() + "\n");
                }
                cnt++;

            }
            listFile = folderSpam.listFiles();
            for (int i = 0; i < listFile.length; i++) {
                File file = listFile[i];
                String content = "";
                //Create a list word for current file
                List<Word> listWordInFile = new ArrayList<>();
                try {
                    content = readFile(file);
                    //Split into words
                    String words[] = content.split("\\W+|\\d+");
                    for (String word : words) {
                        if (word.length() > 1) {
                            Word w = getWordInListWordInFile(word, listWordInFile);
                            if (w == null) {
                                //If word is in word list then search in dictionary for the id
                                int id = getIdFromDictionary(word);
                                if (id >= 0) {
                                    w = new Word(id, word, 1);
                                    listWordInFile.add(w);
                                }
                            } else {
                                w.setTime(w.getTime() + 1);
                            }
                        }
                    }
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
                //Write to file
                for (Word w : listWordInFile) {
                    fw.write((cnt) + " " + (w.getId() + 1) + " " + w.getTime() + "\n");
//                    System.out.print((cnt ) + " " + w.getId() + 1 + " " + w.getTime() + "\n");
                }
                cnt++;

            }

            fw.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    //Return word in the list if exist, else return null
    private Word getWordInListWordInFile(String word, List<Word> listWordInFile) {
        for (Word w : listWordInFile) {
            if (w.getWord().equalsIgnoreCase(word)) {
                return w;
            }
        }
        return null;
    }

    private String readFile(File file) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String res = reader.lines().collect(Collectors.joining(System.lineSeparator()));
        reader.close();
        return res;
    }

    //Return id of the word in dictionary
    private int getIdFromDictionary(String word) {
        for (int i = 0; i < listWord.size(); i++) {
            String w = listWord.get(i);
            if (w.equalsIgnoreCase(word)) {
                return i;
            }
        }
        return -1;
    }

    public static void main(String[] args) {
//        Convert convert = new Convert();
//        convert.readDictionary();
//        convert.convertToFeatureList();
//        convert.writeLabel();
    }
}
